﻿using MGCERP.Common.ViewModel;
using MGCERP.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.BLL
{
    public class MenuBLL
    {
        public Guid GetUserid(string userid)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                Guid Loginid = (Guid)db.CompanyUsers.Where(x => x.AspNetUsersId == userid).Select(a => a.Id).FirstOrDefault();
                return Loginid;
            }

        }
        public List<Guid> GetCompanyId(string userid)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                var Loginid = db.CompanyUsers.Where(x => x.AspNetUsersId == userid).Select(a => a.CompanyId).ToList();
                return Loginid;
            }

         }        
        public List<Guid> GetListMenuId(List<Guid> id)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                var menumodel = new List<Guid>();
                foreach (var item in id)
                {
                    var dta = db.MenusCompanyModules.Where(x => x.CompanyModuleId == item).Select(x => x.MenuId).ToList();
                    menumodel.AddRange(dta);
                }
                return menumodel;
            }

        }
        public List<Guid> GetModuleId(List<Guid> companyId)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                var menumodel = new List<Guid>();
                foreach (var item in companyId)
                {
                    var dta = db.CompaniesCompanyModules.Where(x => x.CompanyId == item).Select(x => x.CompanyModuleId).ToList();
                    menumodel.AddRange(dta);
                }
                return menumodel;
            }

        }
        public List<MenuModuleModel> GetMenuModuleId(Guid moduleid)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                var data = db.MenusCompanyModules.Where(x => x.CompanyModuleId == moduleid).Select(x => new MenuModuleModel()
                {
                    MenuId = x.MenuId,
                    MenuName = db.Menus.Where(a => a.Id == x.MenuId).Select(a=>a.MenuName).FirstOrDefault(),
                }).ToList();

                return data;
            }
          
        }
        public Guid GetCompanyModuleId(Guid companyid)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                var Loginid = db.CompaniesCompanyModules.Where(x => x.CompanyId == companyid).Select(a => a.CompanyModuleId).FirstOrDefault();
                return Loginid;
            }

        }
        public List<Guid> GetMenuId(Guid userid)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                var dta = db.MenusCompanyModules.Where(x => x.CreatedByUserId == userid).Select(x => x.MenuId).ToList();
                return dta;
            }

        }
        public List<Guid> MenuRoleId(List<Guid> MenuId)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                var menumodel = new List<Guid>();
                menumodel = db.MenusAspNetRoles
                               .Where(t => MenuId.Contains(t.MenuId)).Select(x => x.AspNetRoleId).Distinct().ToList();
                //foreach (var item in MenuId)
                //{
                    
                //    var dta = db.MenusRoles.Where(x => x.MenusId == item ).Select(x => x.AspNetRolesId).Distinct().ToList();
                //    menumodel.AddRange(dta);
                //}
                return menumodel;
            }

        }
        public List<Guid> MenuIdfromRole(List<string> RolesId)
        {
            var GuidRolesId = RolesId.Select(Guid.Parse).ToList();
            MGCERP1 db = new MGCERP1();
            var userProfiles = db.MenusAspNetRoles
                               .Where(t => GuidRolesId.Contains(t.AspNetRoleId));
            using (MGCERP1 db1 = new MGCERP1())
            {
                var menumodel = new List<Guid>();                        
                menumodel = db.MenusAspNetRoles
                               .Where(t => GuidRolesId.Contains(t.AspNetRoleId)).Select(x => x.MenuId).Distinct().ToList();
                //foreach (var item in RolesId)
                //{
                //    var dta = db1.MenusRoles.Where(x => x.AspNetRolesId == item).Select(x => x.MenusId).ToList();
                //    menumodel.AddRange(dta);
                //}
                return menumodel;
            }

        }

        public List<Guid> MenuRoleId1(Guid userid)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                var dta = db.MenusCompanyModules.Where(x => x.CreatedByUserId == userid).Select(x => x.MenuId).ToList();
                return dta;
            }
        }
        public List<MenuRolesModel> UserMenuRole(Guid userid)
        {
            List<MenuRolesModel> model = new List<MenuRolesModel>();
            using (MGCERP1 db = new MGCERP1())
            {
                var dta = db.MenusAspNetRoles.Where(x => x.CreatedByUserId == userid).Select(x=>new MenuRolesModel()
                {
                    MenuId=x.MenuId,
                    AspNetRoleId=(Guid)x.AspNetRoleId,
                }).ToList();
                return dta;
            }
        }
        public List<MainMenuModel> MenuRoleName(List<Guid> MenuId)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                var menumodel = new List<MainMenuModel>();
                foreach (var item in MenuId)
                {                   
                    var dta = db.Menus.Where(x => x.Id == item && x.ParentNumber == null).Select(x => new MainMenuModel
                    {
                        Id = x.Id,
                        MenuNumber = x.MenuNumber,
                        MenuName = x.MenuName,
                        ParentId = x.ParentNumber
                     }).ToList();
                    menumodel.AddRange(dta);
                }
                return menumodel;
            }

        }
        public static string GetMenu(Guid id)
        {            
            using (var db = new MGCERP1())
            {
                var data = (from a in db.Menus where a.Id == id select a.MenuName).FirstOrDefault();
                return data;
            }            
        }
        public static string GetRole(string id)
        {
            using (var db = new MGCERP1())
            {
                var data = (from a in db.AspNetRoles where a.Id == id select a.Name).FirstOrDefault();
                return data;
            }
        }
        
        public List<MainMenuModel> getsubmenu(List<MainMenuModel> mainModel)
        {
            List<MainMenuModel> model = new List<MainMenuModel>();
            using (var db = new MGCERP1())
            {
                foreach (var item in mainModel)
                {
                    int ab = (int)item.MenuNumber;
                    var rec = db.Menus.Where(x => x.ParentNumber == ab && x.ParentNumber > 0).Select(x => new MainMenuModel { Id = x.Id, MenuName = x.MenuName, ParentId = x.ParentNumber }).ToList();
                    model.AddRange(rec);
                }

            }
            return model;
        }      
        public static List<MenuModel> GetMenuList()
        {

            using (var db = new MGCERP1())
            {
                var data = db.Menus.Select(x => new MenuModel()
                {
                    Id = x.Id,

                    CreatedByUserId = x.CreatedByUserId,
                    createdByUserName = db.CompanyUsers.Where(a => a.Id == x.CreatedByUserId).Select(a => a.Firstname).FirstOrDefault(),
                    CreatedOn = x.CreatedOn,
                    MenuNumber = (int)x.MenuNumber,
                    MenuName = x.MenuName,
                    ParentNumber = x.ParentNumber,
                    SortOrder = x.SortOrder,
                    IsActive=x.IsActive,

                }).ToList();
                //var ec = data.Select(t => t.CreatedByUserId).Distinct().ToList();
                //List<string> ids = new List<string>();
                //foreach (var item in ec)
                //{
                //    var id = db.CompanyUsers.Where(z => z.Id == item).Select(z=>z.AspNetUsersId).FirstOrDefault();
                //    ids.Add(id);
                //}
                //List<string> names = new List<string>();
                //foreach(var item2 in ids)
                //{
                //    var name = db.AspNetUsers.Where(c => c.Id == item2).Select(c => c.UserName).FirstOrDefault();
                //    names.Add(name);
                //}
                return data;
            }
        }
        public static List<CompanyUserModel> GetCompanyUserList()
        {

            using (var db = new MGCERP1())
            {
                var data = db.CompanyUsers.Select(x => new CompanyUserModel()
                {
                    Id=x.Id,
                    FullName=x.Firstname + x.Lastname
                }).ToList();               
                return data;
            }
        }
        public static List<AspRoleModel> GetRoleList()
        {
            using (var db = new MGCERP1())
            {
                var data = db.AspNetRoles.Select(x => new AspRoleModel()
                {
                    Id = x.Id,
                    Name=x.Name,
                    IsActive=x.IsActive,
                }).ToList();
                return data;
            }
        }
        public static List<MenuRolesModel> GetMenuRoleList()
        {
            using (var db = new MGCERP1())
            {
                var data = db.MenusAspNetRoles.Select(x => new MenuRolesModel()
                {
                   AspNetRoleId=x.AspNetRoleId,
                   RoleName=db.AspNetRoles.Where(a=>a.Id==x.AspNetRoleId.ToString()).Select(a=>a.Name).FirstOrDefault(),
                   MenuId=x.MenuId,
                   MenuName=db.Menus.Where(b=>b.Id==x.MenuId).Select(b=>b.MenuName).FirstOrDefault(),
                   CreatedByUserId=(Guid)x.CreatedByUserId,
                   createdByUserName = db.CompanyUsers.Where(a => a.Id == x.CreatedByUserId).Select(a => a.Firstname).FirstOrDefault(),
                   //IsActive=(bool)x.IsActive,
                }).ToList();
                return data;
            }
        }
        public static bool CreateMenu(MenuModel model, Guid CreatedByUserId)
        {
            bool result = false;
            int menuno = Convert.ToInt32(model.MenuNumber);
            
            using (var db1 = new MGCERP1())
            {

                var qqq = db1.Menus.OrderByDescending(x => x.MenuNumber).ToList();

                if (qqq.Count() != 0)
                {
                    menuno = qqq.FirstOrDefault().MenuNumber + 1;


                }
                else {
                    menuno = 1;
                }
            }
                using (var db = new MGCERP1())
            {
                var menu = new Menu()
                {
                    Id=Guid.NewGuid(),
                    MenuName=model.MenuName,
                    ParentNumber=model.ParentNumber,
                    CreatedByUserId= CreatedByUserId,
                    CreatedOn=DateTime.Now.Date,
                    MenuNumber= menuno, 
                    SortOrder=model.SortOrder,
                    UpdatedByUserId=model.UpdatedByUserId,
                    UPdatedOn = model.UPdatedOn,
                    IsActive=model.IsActive,
                };
                db.Menus.Add(menu);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static bool CreateModule(ModuleModel model, Guid Id)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var module = new CompanyModule()
                {                  
                    Id=Guid.NewGuid(),
                    ModuleName = model.ModuleName,
                    CreatedByUserId=Convert.ToString(Id),
                    CreatedOn=DateTime.Now,
                    IsActive=model.IsActive,
                };
                db.CompanyModules.Add(module);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static MenuModel GetMenuById(Guid id)
        {
            MenuModel model = new MenuModel();

            using (var db = new MGCERP1())
            {
                var data = db.Menus.Where(x =>  x.Id == id).FirstOrDefault();
                model.Id = data.Id;
                model.CreatedByUserId = data.CreatedByUserId;
                model.CreatedOn = data.CreatedOn;
                model.MenuNumber = (int)data.MenuNumber;
                model.MenuName = data.MenuName;
                model.ParentNumber = data.ParentNumber;
                model.UpdatedByUserId = data.UpdatedByUserId;
                model.UPdatedOn = data.UPdatedOn;
            }
            return model;
        }
        public static ModuleModel GetModuleById(Guid id)
        {
            ModuleModel model = new ModuleModel();

            using (var db = new MGCERP1())
            {
                var data = db.CompanyModules.Where(x => x.Id == id).FirstOrDefault();
                model.Id = data.Id;
                model.ModuleName = data.ModuleName;
                model.IsActive = data.IsActive;
            }
            return model;
        }
        public static bool EditModule(ModuleModel model)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.CompanyModules.Where(x => x.Id == model.Id).FirstOrDefault();

                data.ModuleName =model.ModuleName;
                data.IsActive = model.IsActive;
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static MenuRolesModel GetRoleByMenuId(Guid id)
        {
            MenuRolesModel model = new MenuRolesModel();

            using (var db = new MGCERP1())
            {
                var data = db.MenusAspNetRoles.Where(x => x.MenuId == id).FirstOrDefault();
                if (data != null)
                {
                    model.AspNetRoleId = data.AspNetRoleId;
                    model.MenuId = data.MenuId;
                }
                
            }
            return model;
        }
        public  static Guid GetRoleByMenu(Guid id)
        {
            using (var db = new MGCERP1())
            {
                var data = db.MenusAspNetRoles.Where(x => x.MenuId == id).Select(a => a.AspNetRoleId).FirstOrDefault();
                return data;
            }
        }
        //public static AspRoleModel GetAspRoleId(string id)
        //{
        //    AspRoleModel model = new AspRoleModel();
        //    using (var db = new MGCERP1())
        //    {
               
        //        var data = db.AspNetRoles.Where(x => x.Id == id).FirstOrDefault();
        //        model.Id = data.Id;
        //        model.Name = data.Name;
        //        return model;
        //    }
        //}

        //public static bool EditAspRole(AspRoleModel model)
        //{
        //    bool result = false;
        //    using (var db = new MGCERP1())
        //    {
        //        var data = db.AspNetRoles.Where(x => x.Id == model.Id).FirstOrDefault();
        //        {
        //            data.Id = model.Id;
        //            data.Name = model.Name;
        //        }
        //        db.SaveChanges();
        //        result = true;
        //    }
        //    return result;
        //}
        public static string GetAspRoleName(Guid userid)
        {
            string useridd = Convert.ToString(userid);
            using (MGCERP1 db = new MGCERP1())
            {
                var name = db.AspNetRoles.Where(x => x.Id == useridd).Select(a => a.Name).FirstOrDefault();
                return name;
            }
        }
        public static bool CreateRole(MenuRolesModel model,Guid role,Guid MenuId)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var module = new MenusAspNetRole()
                {
                    MenuId= MenuId,
                    CreatedByUserId = model.CreatedByUserId,
                   CreatedOn=DateTime.Now,
                   AspNetRoleId = role,                    
                };
                db.MenusAspNetRoles.Add(module);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static bool CreateMenuRole(MenuRolesModel model)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                bool exist = db.MenusAspNetRoles.Any(x => x.AspNetRoleId == model.AspNetRoleId && x.MenuId==model.MenuId);
                if(exist)
                {
                    result = true;
                }
                else
                { 
                var module = new MenusAspNetRole()
                {
                    MenuId = model.MenuId,
                    CreatedByUserId = model.CreatedByUserId,
                    CreatedOn = DateTime.Now,
                    AspNetRoleId = model.AspNetRoleId,    
                    IsActive=model.IsActive,
                };
             
                //bool exist= db.MenusAspNetRoles.Any(x)
                db.MenusAspNetRoles.Add(module);
                db.SaveChanges();
                    }
                result = true;
            }
            return result;
        }
        public static MenuRolesModel GetRoleByMenuId1(Guid id)
        {
            MenuRolesModel model = new MenuRolesModel();

            using (var db = new MGCERP1())
            {
                var data = db.MenusAspNetRoles.Where(x => x.AspNetRoleId == id).FirstOrDefault();              
                model.MenuId = data.MenuId;
                model.CreatedByUserId = data.CreatedByUserId;

            }
            return model;
        }
        public static bool EditRole(MenuRolesModel model,Guid role)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                bool exist = db.MenusAspNetRoles.Any(x => x.MenuId == model.MenuId);
                if (exist)
                {
                    var data = db.MenusAspNetRoles.Where(x => x.MenuId == model.MenuId).FirstOrDefault();
                    {
                        data.AspNetRoleId = role;
                        data.CreatedByUserId = model.CreatedByUserId;
                    }
                }
                else
                {
                    var module = new MenusAspNetRole()
                    {
                        MenuId = (Guid)model.MenuId,
                        CreatedByUserId = model.CreatedByUserId,
                        CreatedOn = DateTime.Now,
                        AspNetRoleId = role                       
                    };
                    db.MenusAspNetRoles.Add(module);
                }
               db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static bool EditMenu(MenuModel model)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.Menus.Where(x => x.Id == model.Id).FirstOrDefault();

                data.MenuNumber = (int)model.MenuNumber;
                data.MenuName = model.MenuName;
                data.ParentNumber = model.ParentNumber;
                data.SortOrder = model.SortOrder;
                data.IsActive = model.IsActive;
                data.UpdatedByUserId = model.UpdatedByUserId;
                data.UPdatedOn = model.UPdatedOn;
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static bool DeleteRoleMnu(Guid id, Guid RoleId)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.MenusAspNetRoles.Where(x => x.MenuId ==id && x.AspNetRoleId==RoleId).FirstOrDefault();             
                db.MenusAspNetRoles.Remove(data);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static bool DeleteMenu(Guid id)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.Menus.Where(x => x.Id == id).FirstOrDefault();
                db.Menus.Remove(data);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static bool DeleteModule(Guid id)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.CompanyModules.Where(x => x.Id == id).FirstOrDefault();
                db.CompanyModules.Remove(data);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static bool DeleteRole(string id)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.AspNetRoles.Where(x => x.Id == id).FirstOrDefault();
                db.AspNetRoles.Remove(data);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static List<AspRoleModel> RolesListbyMenu(Guid Id)
        {
            using (var db = new MGCERP1())
            {
                var data = db.MenusAspNetRoles.Where(x => x.MenuId == Id && x.IsActive == true).Select(x => new AspRoleModel()
                {
                    Id = x.AspNetRoleId.ToString(),
                    Name = db.AspNetRoles.Where(ab => ab.Id == x.AspNetRoleId.ToString()).Select(a => a.Name).FirstOrDefault(),
                }).ToList();
                return data;
            }
        }
        public static List<MenuModel> GetMenuListByActive(bool isActive)
        {

            using (var db = new MGCERP1())
            {
                var data = db.Menus.Select(x => new MenuModel()
                {
                    Id = x.Id,
                    CreatedByUserId = x.CreatedByUserId,
                    createdByUserName = db.CompanyUsers.Where(a => a.Id == x.CreatedByUserId).Select(a => a.Firstname).FirstOrDefault(),
                    CreatedOn = x.CreatedOn,
                    MenuNumber = x.MenuNumber,
                    MenuName = x.MenuName,
                    ParentNumber = x.ParentNumber,
                    SortOrder = x.SortOrder,
                    IsActive = x.IsActive

                }).ToList();
                if (isActive)
                {
                    data = data.Where(a => a.IsActive == isActive).ToList();

                    return data;
                }
                else
                {
                    data = data.ToList();
                    return data;
                }
            }
        }
        public static List<CompanyModuleModel> ModulesListbyMenu(Guid Id)
        {
            using (var db = new MGCERP1())
            {
                var data = db.MenusCompanyModules.Where(x => x.MenuId == Id && x.IsActive == true).Select(x => new CompanyModuleModel()
                {
                    Id = x.MenuId,
                    ModuleName = db.CompanyModules.Where(ab => ab.Id == x.CompanyModuleId).Select(a => a.ModuleName).FirstOrDefault(),
                }).ToList();
                return data;
            }
        }
        public static bool UpdateRoleCeate(MenuModel model, Guid id)
        {

            bool result = false;
            using (var db = new MGCERP1())
            {
                bool exist = db.MenusAspNetRoles.Any(x => x.AspNetRoleId == new Guid(model.Rolemodel.Id) && x.MenuId == id);
                if (exist)
                {
                    result = true;
                }
                else
                {
                    MenusAspNetRole netmodel = new MenusAspNetRole();
                    netmodel.MenuId = id;
                    netmodel.AspNetRoleId = new Guid(model.Rolemodel.Id);
                    netmodel.CreatedByUserId = model.CreatedByUserId;
                    netmodel.CreatedOn = model.CreatedOn;
                    netmodel.IsActive = model.Rolemodel.IsActive;
                    db.MenusAspNetRoles.Add(netmodel);
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        public static bool UpdateCompanyModuleCeate(MenuModel model, Guid id)
        {

            bool result = false;
            using (var db = new MGCERP1())
            {
                bool exist = db.MenusCompanyModules.Any(x => x.CompanyModuleId == (model.companymodulr.Id) && x.MenuId == id);
                if (exist)
                {
                    result = true;
                }
                else
                {
                    MenusCompanyModule netmodel = new MenusCompanyModule();
                    netmodel.MenuId = id;
                    netmodel.CompanyModuleId = (model.companymodulr.Id);
                    netmodel.CreatedByUserId = model.CreatedByUserId; netmodel.CreatedOn = model.CreatedOn; netmodel.IsActive = model.companymodulr.IsActive;
                    db.MenusCompanyModules.Add(netmodel);
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        public static List<CompanyModuleModel> GetModuleKLisr()
        {
            using (var db = new MGCERP1())
            {
                var data = db.CompanyModules.Select(x => new CompanyModuleModel()
                {
                    Id = x.Id,
                    ModuleName = x.ModuleName
                }).ToList();
                return data;
            }
        }
        public static bool MenusCompanyModulesDelete(Guid menuid, Guid ModuleId)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.MenusCompanyModules.Where(x => x.MenuId == menuid && x.CompanyModuleId== ModuleId).FirstOrDefault();
                db.MenusCompanyModules.Remove(data);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static AspRoleModel GetAspRoleId(string id)
        {
            AspRoleModel model = new AspRoleModel();
            using (var db = new MGCERP1())
            {

                var data = db.AspNetRoles.Where(x => x.Id == id).FirstOrDefault();
                // model.Id = data.Id;
                model.IsActive = data.IsActive;
                model.Name = data.Name;
                return model;
            }
        }

        public static bool EditAspRole(AspRoleModel model)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.AspNetRoles.Where(x => x.Id == model.Id).FirstOrDefault();
                {
                    data.Id = model.Id;
                    data.Name = model.Name;
                    data.IsActive = model.IsActive;
                }
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static List<CompanyModuleModel> GetModuleListByActive(bool isActive)
        {

            using (var db = new MGCERP1())
            {
                var data = db.CompanyModules.Select(x => new CompanyModuleModel()
                {
                    Id = x.Id,
                    ModuleName = x.ModuleName,
                    // createdByUserName = db.CompanyUsers.Where(a => a.Id == x.CreatedByUserId).Select(a => a.Firstname).FirstOrDefault(),
                    CreatedOn = x.CreatedOn,

                    IsActive = x.IsActive

                }).ToList();
                if (isActive)
                {
                    data = data.Where(a => a.IsActive == isActive).ToList();

                    return data;
                }
                else
                {
                    data = data.ToList();
                    return data;
                }
            }
        }
        public static List<AspRoleModel> GetRoleListByActive(bool isActive)
        {

            using (var db = new MGCERP1())
            {
                var data = db.AspNetRoles.Select(x => new AspRoleModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    // createdByUserName = db.CompanyUsers.Where(a => a.Id == x.CreatedByUserId).Select(a => a.Firstname).FirstOrDefault(),
                    //CreatedOn = x.CreatedOn,

                    IsActive = x.IsActive

                }).ToList();
                if (isActive)
                {
                    data = data.Where(a => a.IsActive == isActive).ToList();

                    return data;
                }
                else
                {
                    data = data.ToList();
                    return data;
                }
            }
        }
    }
}


















        