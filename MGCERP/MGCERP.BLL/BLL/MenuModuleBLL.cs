﻿using MGCERP.Common.ViewModel;
using MGCERP.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.BLL
{
   public class MenuModuleBLL
    {
        public static List<MenuModuleModel> GetMenuModuleList()
        {
            using (var db = new MGCERP1())
            {
                var data = db.MenusCompanyModules.Select(x => new MenuModuleModel()
                {
                  MenuId=x.MenuId,
                  MenuName=db.Menus.Where(a=>a.Id==x.MenuId).Select(a=>a.MenuName).FirstOrDefault(),
                  CompanyModuleId=x.CompanyModuleId,
                  ModuleName=db.CompanyModules.Where(b=>b.Id==x.CompanyModuleId).Select(b=>b.ModuleName).FirstOrDefault(),
                  CreatedByUserId=x.CreatedByUserId,
                  createdByUserName = db.CompanyUsers.Where(a => a.Id == x.CreatedByUserId).Select(a => a.Firstname).FirstOrDefault(),
                  CreatedOn =x.CreatedOn,
                  UpdatedByUserId=x.UpdatedByUserId,
                  UpdatedOn=x.UpdatedOn,
                  IsActive=x.IsActive,

                }).ToList();
                return data;
            }
        }
        public static List<ModuleModel> GetModuleList()
        {
            using (var db = new MGCERP1())
            {
                var data = db.CompanyModules.Select(x => new ModuleModel()
                {
                    Id = x.Id,
                    ModuleName=x.ModuleName,
                    CreatedByUserId=x.CreatedByUserId,
                    IsActive=x.IsActive,
                  // createdByUserName = db.CompanyUsers.Where(a => a.Id == x.CreatedByUserId).Select(a => a.Firstname).FirstOrDefault(),

                }).ToList();
                return data;
            }
        }        
        public static MenuModel GetMenuName(Guid id)
        {
            MenuModel model = new MenuModel();
            using (var db = new MGCERP1())
            {
                var aa = (from a in db.Menus where a.Id == id select a.MenuName).FirstOrDefault();
               // var data = db.Menus.Where(x => x.Id == id).FirstOrDefault();
                model.MenuName = aa;
                
            }
            return model;
        }       
        public static bool Createmodule(MenuModuleModel model,Guid MenuId,Guid userid)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var module = new MenusCompanyModule()
                {
                    CompanyModuleId = model.CompanyModuleId,
                    MenuId = MenuId,
                    CreatedByUserId = model.CreatedByUserId,
                    CreatedOn=DateTime.Now,
                    UpdatedByUserId=userid,
                };
                db.MenusCompanyModules.Add(module);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static bool Createmodule(MenuModuleModel model,Guid id)
        {
            
            bool result = false;
            
            using (var db = new MGCERP1())
            {
                bool exist = db.MenusCompanyModules.Any(x => x.CompanyModuleId == model.CompanyModuleId && x.MenuId == model.MenuId);
                if (exist)
                {
                    result = true;
                }
                else
                {
                    //var sale = db.MenusCompanyModules.Where(x => x.CompanyModuleId == model.CompanyModuleId).FirstOrDefault(); 
                    var module = new MenusCompanyModule()
                    {
                        CompanyModuleId = model.CompanyModuleId,
                        MenuId = model.MenuId,
                        CreatedByUserId = id,
                        CreatedOn = DateTime.Now,
                        IsActive = model.IsActive,
                        UpdatedByUserId = id,
                    };
                    db.MenusCompanyModules.Add(module);
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }
        public static bool CreateRole(AspRoleModel model)
        {
            bool result = false;           
            using (var db = new MGCERP1())
            {
                var role = new AspNetRole()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name=model.Name,
                    IsActive=model.IsActive,
                    CreatedByUserId=model.CreatedByUserId,
                   
                };
                db.AspNetRoles.Add(role);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static AspRoleModel GetRoleId(string id)
        {
            AspRoleModel model = new AspRoleModel();

            using (var db = new MGCERP1())
            {
                var data = db.AspNetRoles.Where(x => x.Id == id).FirstOrDefault();
                model.Id = data.Id;
                model.Name = data.Name;

            }
            return model;
        }
        public static bool EditRole(AspRoleModel model)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.AspNetRoles.Where(x => x.Id == model.Id).FirstOrDefault();
                {
                    data.Id = model.Id;
                    data.Name = model.Name;
                }
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static MenuModuleModel GetMenuId(Guid id)
        {
            MenuModuleModel model = new MenuModuleModel();

            using (var db = new MGCERP1())
            {
                var data = db.MenusCompanyModules.Where(x => x.CompanyModuleId == id).FirstOrDefault();
                model.CompanyModuleId = data.CompanyModuleId;
                model.MenuId = data.MenuId;
                model.CreatedByUserId = data.CreatedByUserId;
                model.CreatedOn = data.CreatedOn;
                model.UpdatedByUserId = data.UpdatedByUserId;
                model.UpdatedOn = data.UpdatedOn;
            }
            return model;
        }
        public static Guid GetMenuModuleId(Guid id)
        {
            using (MGCERP1 db = new MGCERP1())
            {
                Guid menuid = db.MenusCompanyModules.Where(x => x.CompanyModuleId == id).Select(a => a.MenuId).FirstOrDefault();
                return menuid;
            }

        }
      
        public static string GetMenuModuleName(Guid id)
        {
            using (var db = new MGCERP1())
            {
                var data = (from a in db.Menus where a.Id == id select a.MenuName).FirstOrDefault();
                return data;
            }
        }
        
        public static bool EditMenu(MenuModuleModel model)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.MenusCompanyModules.Where(x => x.CompanyModuleId == model.CompanyModuleId).FirstOrDefault();
                {
                    data.CompanyModuleId = model.CompanyModuleId;
                    data.MenuId = model.MenuId;
                    data.CreatedByUserId = model.CreatedByUserId;
                    data.CreatedOn = model.CreatedOn;
                    data.UpdatedByUserId = model.UpdatedByUserId;
                    data.UpdatedOn = model.UpdatedOn;
                }
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static bool DeleteModule(Guid id)
        {
            bool result = false;
            using (var db = new MGCERP1())
            {
                var data = db.MenusCompanyModules.Where(x => x.MenuId == id).FirstOrDefault();
                db.MenusCompanyModules.Remove(data);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static List<AspRoleModel> GetRoleList()
        {
            using (var db = new MGCERP1())
            {
                var data = db.AspNetRoles.Select(x => new AspRoleModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsActive=x.IsActive,

                }).ToList();
                return data;
            }
        }
        public static MenuModuleModel GetCreatedUserId(Guid id)
        {
            MenuModuleModel model = new MenuModuleModel();

            using (var db = new MGCERP1())
            {
                var data = db.MenusCompanyModules.Where(x => x.CompanyModuleId == id).FirstOrDefault();               
                model.CreatedByUserId = data.CreatedByUserId;

            }
            return model;
        }
    }
}
