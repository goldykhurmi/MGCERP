﻿using MGCERP.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.Common.ViewModel
{
    public class MenuCompanyModel
    {
        public int Id { get; set; }

        public int? MenuId { get; set; }
        public int CompanyType { get; set; }

        //public virtual CompanyType CompanyType1 { get; set; }

        public virtual MenuCompanyModel MenuCompany1 { get; set; }

        public virtual MenuCompanyModel MenuCompany2 { get; set; }
    }
}
