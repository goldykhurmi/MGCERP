﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.Common.ViewModel
{
  public  class MainMenuModel
    {
        public Guid Id { get; set; }
        public string MenuName { get; set; }
        public string Url { get; set; }
        public Nullable<int> MenuOrder { get; set; }
        public Nullable<bool> IsParent { get; set; }
        public Nullable<int> ParentId { get; set; }
        public int? MenuNumber { get; set; }
        public int? ParentNumber { get; set; }
        public string MenuType { get; set; }
        public int menuid { get; set; }
        public byte RoleId { get; set; }

        public virtual List<string> mainList { get; set; }
        public virtual List<MainMenuModel> submenulist { get; set; }
        public virtual List<MainMenuModel> menuModel{ get; set; }
        public virtual List<MainMenuModel> Sublist { get; set; }
    }
}
