﻿using MGCERP.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.Common.ViewModel
{
    public class MenuModuleModel
    {
        public Guid CompanyModuleId { get; set; }

        [Key]
      //  [Column(Order = 1)]
        public Guid MenuId { get; set; }

        public bool IsActive { get; set; }
        public string MenuName { get; set; }
        public Guid? CreatedByUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UpdatedByUserId { get; set; }
        
        public DateTime? UpdatedOn { get; set; }
        public string createdByUserName { get; set; }
        public string ModuleName { get; set; }
        public virtual CompanyModule Module { get; set; }
        public List<MenuModuleModel> menumodulelist { get; set; }
        public List<MenuModel> menulist { get; set; }
        public MenuModuleModel Menumodule { get; set; }
        public MenuModel menumodel { get; set; }
    }
}
