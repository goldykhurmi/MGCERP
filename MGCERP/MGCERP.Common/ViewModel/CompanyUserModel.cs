﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.Common.ViewModel
{
   public class CompanyUserModel
    {
        public Guid Id { get; set; }
        
        public string AspNetUsersId { get; set; }

        public Guid? CompanyId { get; set; }

        public Guid? CompanyTitleId { get; set; }

        public int? GenderPid { get; set; }


        //public string Firstname { get; set; }

        //public string Lastname { get; set; }
        public string FullName { get; set; }
        public Guid? LocationId { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? DepmartmentSubId { get; set; }

        public int? LanguageId { get; set; }

        public DateTime? InDatetime { get; set; }

        public int? InOut { get; set; }

        public bool? IsEnablePremise { get; set; }

        public bool? IsEnableTimeFilter { get; set; }

        public bool? IsOnPremises { get; set; }

        public bool? IsResetPassword { get; set; }

        public bool? IsSyncProduction { get; set; }

        public bool? IsSupportUser { get; set; }

        public bool? IsSales { get; set; }

        public bool? IsActive { get; set; }

        public DateTime? CurrentLoginDate { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public DateTime? LastLogOffDate { get; set; }

        public DateTime? OutDateTime { get; set; }

        public Guid? CreatedbyUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UpdatedByUserId { get; set; }

        public DateTime? UpdatedOn { get; set; }
    }
}
