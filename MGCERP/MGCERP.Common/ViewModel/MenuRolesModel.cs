﻿using MGCERP.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.Common.ViewModel
{
  public  class MenuRolesModel
    {

        public Guid MenuId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid AspNetRoleId { get; set; }

        public bool IsActive { get; set; }
        public string  MenuName { get; set; }
        public Guid? CreatedByUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UpdatedByUserId { get; set; }

        public DateTime? UpdatedOn { get; set; }
        //[StringLength(128)]
        //public Guid AspNetRolesId { get; set; }
        public string RoleName { get; set; }

        //[Key]
        //[Column(Order = 0)]
        //public Guid CreatedbyUserId { get; set; }
        public string createdByUserName { get; set; }
        //[Key]
        //[Column(Order = 1)]
        //[DataType(DataType.Date)]
        //public DateTime CreatedOn { get; set; }

        //public Guid? MenusId { get; set; }

        //public Guid? UpdatedByUserId { get; set; }

        //public DateTime? UpdatedOn { get; set; }

        //    public virtual AspNetRole AspNetRole { get; set; }

        public virtual Menu Menu { get; set; }
        public List<MenuModel> menulist { get; set; }
        public MenuRolesModel menurolemodel { get; set; }
        public List<MenuRolesModel> menurolelist { get; set; }
        public List<AspRoleModel> Rolelist { get; set; }
    }
}
