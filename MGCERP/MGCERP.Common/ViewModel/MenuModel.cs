﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.Common.ViewModel
{
   public class MenuModel
    {
        public MenuModel()
        {
            menulist = new List<MenuModel>();
            menurolelist = new List<MenuRolesModel>();
            companymenumodulelist =new List<CompanyModuleModel>();
            roles = new List<AspRoleModel>();
            compmodule = new List<CompanyModuleModel>(); companymodulr = new CompanyModuleModel();
        }
        public Guid Id { get; set; }

        public int? MenuNumber { get; set; }

      
        public string MenuName { get; set; }

        public int? ParentNumber { get; set; }

        public int? SortOrder { get; set; }

        public Guid? CreatedByUserId { get; set; }

        [DataType(DataType.Date)]
        public DateTime? CreatedOn { get; set; }

        public Guid? UpdatedByUserId { get; set; }

      
        public DateTime? UPdatedOn { get; set; }

        public bool IsActive { get; set; }
        public string createdByUserName { get; set; }
        public List<MenuModel> menulist { get; set; }
        public MenuModel menumodel { get; set; }
        public CompanyModuleModel companymodulr { get; set; }
        public List<MenuRolesModel> menurolelist { get; set; }
        
               public List<AspRoleModel> roles { get; set; }
        public List<CompanyModuleModel> compmodule { get; set; }
        public AspRoleModel Rolemodel { get; set; }
        public List<AspRoleModel> Rolelist { get; set; }
        public List<ModuleModel> modulelist { get; set; }
        public List<CompanyModuleModel> companymenumodulelist { get; set; }
        public List<MenuModuleModel> menumodulelist { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<MenuTranslationModel> MenuTranslations { get; set; }
    }
}
