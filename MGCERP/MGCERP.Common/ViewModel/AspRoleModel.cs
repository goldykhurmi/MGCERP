﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.Common.ViewModel
{
   public class AspRoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string CreatedByUserId { get; set; }

        public List<AspRoleModel> RoleList { get; set; }
        public bool IsActive { get; set; }
    }
}
