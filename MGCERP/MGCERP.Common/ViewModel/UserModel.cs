﻿using MGCERP.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.Common.ViewModel
{
  public class UserModel
    {
        public System.Guid Id { get; set; }
        public Nullable<System.Guid> CompanyId { get; set; }
        public Nullable<System.Guid> CreatedbyUserId { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> CurrentLoginDate { get; set; }
        public Nullable<long> DepartmentId { get; set; }
        public Nullable<long> DepartmentSubId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public Nullable<int> GenderPid { get; set; }
        public Nullable<System.DateTime> InDatetime { get; set; }
        public Nullable<int> InOut { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsEnablePremise { get; set; }
        public Nullable<bool> IsEnableTimeFilter { get; set; }
        public Nullable<bool> IsOnPremises { get; set; }
        public Nullable<bool> IsResetPassword { get; set; }
        public Nullable<bool> IsSupportUser { get; set; }
        public Nullable<bool> IsSyncProduction { get; set; }
        public Nullable<int> LanguageId { get; set; }
        public Nullable<System.DateTime> LastLoginDate { get; set; }
        public Nullable<System.DateTime> LastLogOffDate { get; set; }
        public string Lastname { get; set; }
        public Nullable<long> LocationId { get; set; }
        public Nullable<System.DateTime> OutDateTime { get; set; }
        public Nullable<System.Guid> UpdatedByUserId { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
        public virtual Company Company { get; set; }
        //public virtual Language Language { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<UserDetail> UserDetails { get; set; }
    }
}
