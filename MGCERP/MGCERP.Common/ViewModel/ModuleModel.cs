﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGCERP.Common.ViewModel
{
    public class ModuleModel
    {

        public Guid Id { get; set; }

        [Required]
        [StringLength(25)]
        public string ModuleName { get; set; }

        public bool IsActive { get; set; }

        [StringLength(128)]
        public string CreatedByUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(128)]
        public string UpdatedByUserId { get; set; }

        public string createdByUserName { get; set; }

        public DateTime? UpdatedOn { get; set; }

    }
}
