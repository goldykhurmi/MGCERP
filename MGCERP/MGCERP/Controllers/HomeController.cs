﻿using MGCERP.BLL;
using MGCERP.Common.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MGCERP.Controllers
{
   // [Authorize]
    [Authorize(Roles = "Global Admin")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Index2()
        {
            return View();
        }
        public ActionResult Menus()
        {
            MainMenuModel model = new MainMenuModel();
            var service = new MenuBLL();
            var userid = User.Identity.GetUserId();
            var companyId = service.GetCompanyId(userid);
            var loginUserid = service.GetUserid(userid);
            //var companyId = service.GetCompanyId(loginUserid);
            var ModuleId = service.GetModuleId(companyId);
            var MenuModuleId = service.GetListMenuId(ModuleId);
           // var MenuId = service.GetMenuId(loginUserid);            
            var manager = HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            var usr = manager.UserManager.FindByEmail(User.Identity.GetUserName());
            var rolesid = usr.Roles.Select(d => d.RoleId).ToList();
            
            var MenuRoleId = service.MenuRoleId(MenuModuleId);
            var Menuids = service.MenuIdfromRole(rolesid);
            var MenuName = service.MenuRoleName(Menuids);
            model.menuModel = MenuName;
            var sbmenu = service.getsubmenu(MenuName);
            model.submenulist = sbmenu;
            return PartialView(model);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}