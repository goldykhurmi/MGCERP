﻿using MGCERP.Common.ViewModel;

using MGCERP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MGCERP.BLL;
using Microsoft.AspNet.Identity;

namespace MGCERP.Areas.Admin.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu/Mrnu
        public ActionResult Index()
        {
            MenuModel menu = new MenuModel();
            menu.menulist = MenuBLL.GetMenuList();
            //menu.menurolelist = MenuBLL.GetMenuRoleList();
            //menu.Rolelist = MenuModuleBLL.GetRoleList();
            //menu.modulelist = MenuModuleBLL.GetModuleList();
            // menu.menumodulelist = MenuModuleBLL.GetMenuModuleList();
            
            //var menu1 = MenuBLL.GetMenuList();
            return View(menu);
        }
        public ActionResult MenuRoleIndex()
        {
            var menumodule = MenuBLL.GetMenuRoleList();
            return View(menumodule);
        }
        public ActionResult ModuleCompanyIndex()
        {
            var modulelist = MenuModuleBLL.GetMenuModuleList();
            return View(modulelist);
        }
        public ActionResult RoleIndex()
        {
            var Rolelist = MenuModuleBLL.GetRoleList();
            return View(Rolelist);
        }
        public ActionResult ModuleIndex()
        {
            var modulelist = MenuModuleBLL.GetModuleList();
            return View(modulelist);
        }
        public ActionResult CreateMenu(Guid id, Guid UserId)
        {
            MenuRolesModel model = new MenuRolesModel();
            //ViewBag.menulist = MenuBLL.GetMenuList();
            ViewBag.menulist = new SelectList(MenuBLL.GetMenuList(), "Id", "MenuName");
            ViewBag.Rolelist = new SelectList(MenuModuleBLL.GetRoleList(), "Id", "Name");
            var data = MenuBLL.GetRoleByMenuId1(id);
            var id1 = (Guid)data.MenuId;
            var menuname = MenuBLL.GetMenu(id1);
            ViewBag.name = menuname;
            model.AspNetRoleId = id;
            ViewBag.CreatedbyUserId = UserId;
            model.CreatedByUserId = UserId;
            model.MenuId = data.MenuId;
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateMenu(MenuRolesModel model,Guid role,Guid MenuId)
        {
            if (ModelState.IsValid)
            {
                ViewBag.menulist = new SelectList(MenuBLL.GetMenuList(), "Id", "Name");
                ViewBag.Rolelist = new SelectList(MenuModuleBLL.GetRoleList(), "Id", "Name");
                var userid = User.Identity.GetUserId();
                Guid createduserid = new Guid(userid);
                model.CreatedByUserId = createduserid;
                bool create = MenuBLL.CreateRole(model,role,MenuId);
                if (!create)
                {
                    ViewBag.ErroMessage = "Something Went Wrong to save data";
                }
                else
                {
                    ViewBag.SuccessMessage = "Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        public ActionResult CreateRoleMenu()
        {
            MenuRolesModel model = new MenuRolesModel();
            //ViewBag.menulist = MenuBLL.GetMenuList();
            ViewBag.menulist = new SelectList(MenuBLL.GetMenuList(), "Id", "MenuName");
            ViewBag.Rolelist = new SelectList(MenuModuleBLL.GetRoleList(),"Id","Name");
            
            //var data = MenuBLL.GetRoleByMenuId1(id);
            //model.AspNetRolesId = id;
            //model.CreatedbyUserId = data.CreatedbyUserId;
            //model.MenusId = data.MenusId;
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateRoleMenu(MenuRolesModel model)
        {
            if (ModelState.IsValid)
            {
                ViewBag.menulist = new SelectList(MenuBLL.GetMenuList(), "Id", "MenuName");
                ViewBag.Rolelist = new SelectList(MenuModuleBLL.GetRoleList(), "Id", "Name");
                bool create = MenuBLL.CreateMenuRole(model);
               
                if (!create)
                {
                    ViewBag.ErroMessage = "Something Went Wrong to save data";
                }
                else
                {
                    ViewBag.SuccessMessage = "Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        
        public ActionResult Create()
        {
            ViewBag.UsersList = new SelectList(MenuBLL.GetCompanyUserList(), "Id", "FullName");
            return View();
        }

        [HttpPost]
        public ActionResult Create(MenuModel model)
        {
            if (ModelState.IsValid)
            {
                model.MenuNumber = 0;
                   var userid = User.Identity.GetUserId();
                Guid Id = new Guid(userid);
                ViewBag.UsersList = new SelectList(MenuBLL.GetCompanyUserList(), "Id", "FullName");

                model.menulist = MenuBLL.GetMenuList();
                bool create = MenuBLL.CreateMenu(model, Id);
                if (!create)
                {
                    ViewBag.ErroMessage = "Something Went Wrong to save data";
                }
                else
                {
                    ViewBag.SuccessMessage = "Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        public ActionResult CreateModule(string id)
        {
            ModuleModel model = new ModuleModel();
            ViewBag.UsersList = new SelectList(MenuBLL.GetCompanyUserList(), "Id", "FullName");
           
            //model.CreatedByUserId = id;
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateModule(ModuleModel model)
        {
            if (ModelState.IsValid)
            {
                var userid = User.Identity.GetUserId();
                Guid Id = new Guid(userid);
                bool create = MenuBLL.CreateModule(model, Id);
                if (!create)
                {
                    ViewBag.ErroMessage = "Something Went Wrong to save data";
                }
                else
                {
                    ViewBag.SuccessMessage = "Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            else
            {

            }
            return View(model);
        }
        public ActionResult CreateMenuModule()
        {
            MenuModuleModel model = new MenuModuleModel();
            ViewBag.menulist = new SelectList(MenuBLL.GetMenuList(), "Id", "MenuName");
            ViewBag.modulelist = new SelectList(MenuModuleBLL.GetModuleList(), "Id", "ModuleName");
            //ViewBag.UsersList = new SelectList(MenuBLL.GetCompanyUserList(), "Id", "FullName");                                
            return View(model);
        }

        [HttpPost]
        //public ActionResult CreateMenuModule(MenuModuleModel model,Guid ModuleId,Guid CreatedByUserId)
               public ActionResult CreateMenuModule(MenuModuleModel model)
        {
            if (ModelState.IsValid)
            {
                ViewBag.menulist = new SelectList(MenuBLL.GetMenuList(), "Id", "MenuName");
                model.CompanyModuleId = model.CompanyModuleId;                
                var userid = User.Identity.GetUserId();
                Guid Id = new Guid(userid);
                bool create = MenuModuleBLL.Createmodule(model,Id);
                if (!create)
                {
                    ViewBag.ErroMessage = "Something Went Wrong to save data";
                }
                else
                {
                    ViewBag.SuccessMessage = "Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        public ActionResult EditRole(Guid id,Guid UserId)
        {
            MenuRolesModel menu = new MenuRolesModel();
            ViewBag.RoleList = new SelectList(MenuBLL.GetRoleList(), "Id", "Name");
                var role = MenuBLL.GetRoleByMenu(id);
                var rolename = MenuBLL.GetAspRoleName(role);
                menu.AspNetRoleId =role;
                menu.RoleName = rolename;
                menu.CreatedByUserId = UserId;
                menu.MenuId = id;
                return View(menu);
            
        }
        [HttpPost]
        public ActionResult EditRole(MenuRolesModel model,Guid role)
        {
            if (ModelState.IsValid)
            {
                bool menu = MenuBLL.EditRole(model, role);
                if (!menu)
                {
                    ViewBag.ErroMessage = "Something Went Wrong";
                }
                else
                {
                    ViewBag.SuccessMessage = " Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        public ActionResult EditAspRole(string id)
        {
            var menu = MenuBLL.GetAspRoleId(id);

            return View(menu);

        }
        [HttpPost]
        public ActionResult EditAspRole(AspRoleModel model)
        {
            if (ModelState.IsValid)
            {
                bool menu = MenuBLL.EditAspRole(model);
                if (!menu)
                {
                    ViewBag.ErroMessage = "Something Went Wrong";
                }
                else
                {
                    ViewBag.SuccessMessage = " Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        
        public ActionResult Edit(Guid id)
        {
                var menu = MenuBLL.GetMenuById(id);

                return View(menu);
       
        }
        [HttpPost]
        public ActionResult Edit(MenuModel model)
        {
            if (ModelState.IsValid)
            {
                bool menu = MenuBLL.EditMenu(model);
                if (!menu)
                {
                    ViewBag.ErroMessage = "Something Went Wrong";
                }
                else
                {
                    ViewBag.SuccessMessage = " Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        //public ActionResult Delete(Guid id)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        bool menu = MenuBLL.Delete(id);
        //        if (!menu)
        //        {
        //            ViewBag.ErroMessage = "Something Went Wrong";
        //        }
        //        else
        //        {
        //            ViewBag.SuccessMessage = " Record Saved Successfully.";
        //            return RedirectToAction("Index");
        //        }
        //    }
        //    return View();
        //}
        public ActionResult DeleteModule(Guid id)
        {
            if (ModelState.IsValid)
            {
                bool menu = MenuBLL.DeleteModule(id);
                if (!menu)
                {
                    ViewBag.ErroMessage = "Something Went Wrong";
                }
                else
                {
                    ViewBag.SuccessMessage = " Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        public ActionResult DeleteAspRole(string id)
        {
            if (ModelState.IsValid)
            {
                bool menu = MenuBLL.DeleteRole(id);
                if (!menu)
                {
                    ViewBag.ErroMessage = "Something Went Wrong";
                }
                else
                {
                    ViewBag.SuccessMessage = " Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        public ActionResult DeleteMenuModule(Guid id)
        {
            if (ModelState.IsValid)
            {
                bool menu = MenuModuleBLL.DeleteModule(id);
                if (!menu)
                {
                    ViewBag.ErroMessage = "Something Went Wrong";
                }
                else
                {
                    ViewBag.SuccessMessage = " Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        public ActionResult DeleteMenu(Guid id)
        {
            if (ModelState.IsValid)
            {
                bool menu = MenuBLL.DeleteMenu(id);
                if (!menu)
                {
                    ViewBag.ErroMessage = "Something Went Wrong";
                }
                else
                {
                    ViewBag.SuccessMessage = " Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
       
        public ActionResult RoleCreate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RoleCreate(AspRoleModel model)
        {
            if (ModelState.IsValid)
            {
                var uid = User.Identity.GetUserId();
               // Guid Createduserid = new Guid(uid);
                model.CreatedByUserId = uid;
                bool create = MenuModuleBLL.CreateRole(model);
                if (!create)
                {
                    ViewBag.ErroMessage = "Something Went Wrong to save data";
                }
                else
                {
                    ViewBag.SuccessMessage = "Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        public ActionResult ModuleMenu(Guid id,Guid UserId)
        {
             MenuModuleModel model = new MenuModuleModel();
            ViewBag.menulist = new SelectList(MenuBLL.GetMenuList(), "Id", "MenuName");
            var menu = MenuModuleBLL.GetMenuModuleId(id);
           var menuname = MenuModuleBLL.GetMenuModuleName(menu);
            model.CompanyModuleId = id;
            model.CreatedByUserId = UserId;
            ViewBag.Menu = menuname;
            //model.Menumodule = menu;
            return View(model);
        }

        [HttpPost]
        public ActionResult ModuleMenu(MenuModuleModel model,Guid MenuId)
        {
            if (ModelState.IsValid)
            {
                ViewBag.menulist = new SelectList(MenuBLL.GetMenuList(), "Id", "MenuName");
                bool create = MenuModuleBLL.Createmodule(model, MenuId);
                if (!create)
                {
                    ViewBag.ErroMessage = "Something Went Wrong to save data";
                }
                else
                {
                    ViewBag.SuccessMessage = "Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        
        public ActionResult Menuname(Guid id)
        {
            MenuModel model = new MenuModel();
            var modulelist = MenuModuleBLL.GetMenuName(id);
            model.menumodel = modulelist;
            return View(model);
        }
        public ActionResult EditMenu(Guid Id)
        {
            MenuModel model = new MenuModel();
            model = MenuBLL.GetMenuById(Id);
            model.Rolelist = MenuBLL.RolesListbyMenu(Id);
            model.companymenumodulelist = MenuBLL.ModulesListbyMenu(Id);

            return View(model);
        }
        [HttpPost]
        public ActionResult EditMenu(MenuModel model)
        {
            if (ModelState.IsValid)
            {
                var id = User.Identity.GetUserId();
                Guid updatedid = new Guid(id);
                model.UpdatedByUserId = updatedid;
                model.UPdatedOn = DateTime.Now;
                bool menu = MenuBLL.EditMenu(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        //public ActionResult EditModule(Guid Id)
        //{
        //    ModuleModel model = new ModuleModel(); 
        //    var menu = MenuBLL.GetModuleById(Id);
        //    return View(model);
        //}
        //[HttpPost]
        //public ActionResult EditModule(ModuleModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var id = User.Identity.GetUserId();
        //        //Guid updatedid = new Guid(id);
        //        //model.UpdatedByUserId = updatedid;
        //        //model.UPdatedOn = DateTime.Now;
        //        var menu = MenuBLL.EditModule(model);
        //        return RedirectToAction("Index");
        //    }
        //    return View(model);
        //}
        [HttpGet]
        public ActionResult ShowActiveMenus(bool check)
        {
            var allrecords = MenuBLL.GetMenuListByActive(check);
            return Json(allrecords, JsonRequestBehavior.AllowGet);

        }
        public ActionResult aspnetRoleCreate()
        {
            var menumodel = new MenuModel();
            menumodel.roles = MenuBLL.GetRoleList();
            return View(menumodel);
        }
        [HttpPost]
        public ActionResult aspnetRoleCreate(Guid id, MenuModel model)
        {
            var uid = User.Identity.GetUserId();
            Guid updatedid = new Guid(uid);
            model.CreatedByUserId = updatedid;
            model.CreatedOn = DateTime.Now;
            var res = MenuBLL.UpdateRoleCeate(model, id);
            return RedirectToAction("Index");
        }

        public ActionResult companyModuleCreate()
        {
            var menumodel = new MenuModel();
            menumodel.compmodule = MenuBLL.GetModuleKLisr();
            return View(menumodel);
        }
        [HttpPost]
        public ActionResult companyModuleCreate(Guid id, MenuModel model)
        {
            var uid = User.Identity.GetUserId();
            Guid updatedid = new Guid(uid);
            model.CreatedByUserId = updatedid;
            model.CreatedOn = DateTime.Now;
            var res = MenuBLL.UpdateCompanyModuleCeate(model, id);
            return RedirectToAction("Index");
        }
        public ActionResult aspnetRoleDelete(Guid menuId,Guid Roleid)
        {
            var res = MenuBLL.DeleteRoleMnu(menuId,Roleid);
            return RedirectToAction("Index");
        }

        public ActionResult MenusCompanyModulesDelete(Guid menuId,Guid ModuleId)
        {
            var res = MenuBLL.MenusCompanyModulesDelete(menuId, ModuleId);
            return RedirectToAction("Index");
        }
        // public ActionResult EditAspRole(string id)
        //{
        //    var menu = MenuBLL.GetAspRoleId(id);

        //    return View(menu);
           
        //}
        //[HttpPost]
        //public ActionResult EditAspRole(AspRoleModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        bool menu = MenuBLL.EditAspRole(model);
        //        if (!menu)
        //        {
        //            ViewBag.ErroMessage = "Something Went Wrong";
        //        }
        //        else
        //        {
        //            ViewBag.SuccessMessage = " Record Saved Successfully.";
        //            return RedirectToAction("Index");
        //        }
        //    }
        //    return View(model);
        //}
     public ActionResult EditModule(Guid Id)
        {
            ModuleModel model = new ModuleModel(); 
            var menu = MenuBLL.GetModuleById(Id);
            return View(menu);
        }
        [HttpPost]
        public ActionResult EditModule(ModuleModel model)
        {
            if (ModelState.IsValid)
            {
                //var id = User.Identity.GetUserId();
                //Guid updatedid = new Guid(id);
                //model.UpdatedByUserId = updatedid;
                //model.UPdatedOn = DateTime.Now;
                var menu = MenuBLL.EditModule(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult ShowActiveCompanyModule(bool check)
        {
            var allmodule = MenuBLL.GetModuleListByActive(check);
            return Json(allmodule, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public ActionResult ShowActiveRole(bool check)
        {
            var allrole = MenuBLL.GetRoleListByActive(check);
            return Json(allrole, JsonRequestBehavior.AllowGet);

        }
    }
}