﻿using MGCERP.BLL;
using MGCERP.Common.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MGCERP.Areas.Admin.Controllers
{
    public class MenuModuleController : Controller
    {
        // GET: Menu/MenuModule
        public ActionResult Index()
        {
            var modulelist = MenuModuleBLL.GetMenuModuleList();
            return Json(modulelist);
        }
        
         public ActionResult Menu(Guid id)
         {
            var modulelist = MenuModuleBLL.GetMenuName(id);
            return View(modulelist);
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AspRoleModel model)
        {
            if (ModelState.IsValid)
            {
                bool create = MenuModuleBLL.CreateRole(model);
                if (!create)
                {
                    ViewBag.ErroMessage = "Something Went Wrong to save data";
                }
                else
                {
                    ViewBag.SuccessMessage = "Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        public ActionResult CreateMenuModule(Guid id)
        {
            MenuModuleModel model = new MenuModuleModel();
            //ViewBag.menulist = MenuBLL.GetMenuList();
            ViewBag.menulist = new SelectList(MenuBLL.GetMenuList(), "Id", "Name");
            var data = MenuModuleBLL.GetCreatedUserId(id);
            model.CompanyModuleId = id;
            model.CreatedByUserId = data.CreatedByUserId;
            return View(model);
        }

        [HttpPost]
        //public ActionResult CreateMenuModule(MenuModuleModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        ViewBag.menulist = new SelectList(MenuBLL.GetMenuList(), "Id", "Name");
        //        bool create = MenuModuleBLL.Createmodule(model);
        //        if (!create)
        //        {
        //            ViewBag.ErroMessage = "Something Went Wrong to save data";
        //        }
        //        else
        //        {
        //            ViewBag.SuccessMessage = "Record Saved Successfully.";
        //            return RedirectToAction("Index");
        //        }
        //    }
        //    return View(model);
        //}
        
        public ActionResult ModuleIndex()
        {
            var modulelist = MenuModuleBLL.GetModuleList();
            return View(modulelist);
        }
        public ActionResult EditUserRole(string id)
        {

                var menu = MenuModuleBLL.GetRoleId(id);

                return View(menu);
        }
        [HttpPost]
        public ActionResult EditUserRole(AspRoleModel model)
        {
            if (ModelState.IsValid)
            {
                bool role = MenuModuleBLL.EditRole(model);
                if (!role)
                {
                    ViewBag.ErroMessage = "Something Went Wrong";
                }
                else
                {
                    ViewBag.SuccessMessage = " Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        public ActionResult EditMenu(Guid id)
        {
            //ViewBag.RoleList = new SelectList(MenuBLL.GetRoleList(), "Id", "Name");
            //if (id > 0)
            //{
                var menu = MenuModuleBLL.GetMenuId(id);

                return View(menu);
            //}

            //return View();
        }
        [HttpPost]
        public ActionResult EditMenu(MenuModuleModel model)
        {
            if (ModelState.IsValid)
            {
                bool menu = MenuModuleBLL.EditMenu(model);
                if (!menu)
                {
                    ViewBag.ErroMessage = "Something Went Wrong";
                }
                else
                {
                    ViewBag.SuccessMessage = " Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        public ActionResult DeleteMenuModule(Guid id)
        {
            if (ModelState.IsValid)
            {
                bool menu = MenuModuleBLL.DeleteModule(id);
                if (!menu)
                {
                    ViewBag.ErroMessage = "Something Went Wrong";
                }
                else
                {
                    ViewBag.SuccessMessage = " Record Saved Successfully.";
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        public ActionResult RoleIndex()
        {
            var Rolelist = MenuModuleBLL.GetRoleList();
            return View(Rolelist);
        }

    }
}