﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using MGCERP.Models;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Twitter;
using Owin.Security.Providers.LinkedIn;

namespace MGCERP
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });            
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

        //    app.UseTwitterAuthentication(new TwitterAuthenticationOptions
        //     {
        //        ConsumerKey= "74I8wt6RJjqPYB5TVWAIjoLI9",
        //     ConsumerSecret= "MG9TXeV0BiwUFHGAbFFVdfT4PWCb6NlkomORsfcevrLwhJdxBv",
        //     BackchannelCertificateValidator = new Microsoft.Owin.Security.CertificateSubjectKeyIdentifierValidator(new[]
        //{

        //  "A5EF0B11CEC04103A34A659048B21CE0572D7D47", // VeriSign Class 3 Secure Server CA - G2
        //  "0D445C165344C1827E1D20AB25F40163D8BE79A5", // VeriSign Class 3 Secure Server CA - G3
        //  "7FD365A7C2DDECBBF03009F34339FA02AF333133", // VeriSign Class 3 Public Primary CA - G5
        //  "39A55D933676616E73A761DFA16A7E59CDE66FAD", // Symantec Class 3 Secure Server CA - G4
        //  "‎add53f6680fe66e383cbac3e60922e3b4c412bed", // Symantec Class 3 EV SSL CA - G3
        //  "4eb6d578499b1ccf5f581ead56be3d9b6744a5e5", // VeriSign Class 3 Primary CA - G5
        //  "5168FF90AF0207753CCCD9656462A212B859723B", // DigiCert SHA2 High Assurance Server C‎A 
        //  "B13EC36903F8BF4701D498261A0802EF63642BC3"  // DigiCert High Assurance EV Root CA
        //})
        //    });

            //app.UseTwitterAuthentication(new TwitterAuthenticationOptions
            //{
            //    ConsumerKey = "8zqnEPBBhwkRCTfHzM6X4pjjj",
            //    ConsumerSecret = "GiA3VYHN9OiM6BaEtPdjhXWXcocQSCNbqKXgIDuPP6UrqVo8lD"


            //});





            app.UseLinkedInAuthentication(new LinkedInAuthenticationOptions()
            {
                ClientId = "784dw0jia0skzs",
                ClientSecret = "C64tQsENY57fC0qP"

            }
           );

            //app.UseLinkedInAuthentication("<81jikuag90nsbe>", "<wTIv090yyB9vCwGA>");



            //var facebookOptions = new FacebookAuthenticationOptions()
            //{
            //    AppId = "1784575734905550",
            //    AppSecret = "27c344e523af242cb39a13a9186878ac",
            //    UserInformationEndpoint = "https://graph.facebook.com/v2.4/me?fields=id,name,email,first_name,last_name",
            //};

            //facebookOptions.Scope.Add("email");
            //app.UseFacebookAuthentication(facebookOptions);
            //app.UseFacebookAuthentication(
            //   appId: "1430658033648795",
            //   appSecret: "fbc28187c02ab9460df81d3ef81b302f");


            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "248293571564-7n9u4t58917rs2g96q5eejrd5gf3m36d.apps.googleusercontent.com",
            //    ClientSecret = "_ZHKgIKhgXYE7g4aDcVoX2-6"
            //});
        }
    }
}