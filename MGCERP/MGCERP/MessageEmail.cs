﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace MGCERP
{
    public class MessageEmail
    {

        public async static Task SendEmailAsync(string email, string subject, string message)
        {
            string _from = ConfigurationManager.AppSettings["MboxUsername"];
            string _user = ConfigurationManager.AppSettings["MboxUsername"];
            string _password = ConfigurationManager.AppSettings["MboxPassword"];
            string _smtpdetail = ConfigurationManager.AppSettings["Mboxserver"];

            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(_smtpdetail);

                mail.From = new MailAddress(_from);
                mail.To.Add(email);
                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(_user, _password);
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<string> EmailTemplate(string template)
        {
            var test = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/EmailTemplates/");
            var filepath = HostingEnvironment.MapPath("~/Content/EmailTemplates/") + template + ".html";
            StreamReader sr = new StreamReader(filepath);
            var body = await sr.ReadToEndAsync();
            return body;
        }
    }
    }
