﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MGCERP.Startup))]
namespace MGCERP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
