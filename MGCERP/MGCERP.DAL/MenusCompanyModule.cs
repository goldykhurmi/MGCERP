namespace MGCERP.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MenusCompanyModule
    {
        [Key]
        [Column(Order = 0)]
        public Guid CompanyModuleId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid MenuId { get; set; }

        public bool IsActive { get; set; }

        public Guid? CreatedByUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UpdatedByUserId { get; set; }

        public DateTime? UpdatedOn { get; set; }
    }
}
