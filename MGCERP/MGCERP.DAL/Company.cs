namespace MGCERP.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Company
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Company()
        {
            Companies1 = new HashSet<Company>();
            Companies11 = new HashSet<Company>();
            CompanyUsers = new HashSet<CompanyUser>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(80)]
        public string OfficialName { get; set; }

        [StringLength(10)]
        public string ShortName { get; set; }

        public int CompanyTypePid { get; set; }

        public Guid? BillingCompanyId { get; set; }

        public Guid ParentCompanyId { get; set; }

        public Guid? LocationId { get; set; }

        public bool? IsMBox { get; set; }

        public bool? IsSyncProduction { get; set; }

        public bool IsActive { get; set; }

        [StringLength(128)]
        public string CreatedByUserId { get; set; }

        [StringLength(128)]
        public string UpdatedByUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Company> Companies1 { get; set; }

        public virtual Company Company1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Company> Companies11 { get; set; }

        public virtual Company Company2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyUser> CompanyUsers { get; set; }
    }
}
