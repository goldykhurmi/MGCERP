namespace MGCERP.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Menu
    {
        public Guid Id { get; set; }

        public int MenuNumber { get; set; }

        [Required]
        [StringLength(10)]
        public string MenuName { get; set; }

        public int? ParentNumber { get; set; }

        public int? SortOrder { get; set; }

        public Guid? CreatedByUserId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? CreatedOn { get; set; }

        public Guid? UpdatedByUserId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? UPdatedOn { get; set; }

        public bool IsActive { get; set; }
    }
}
