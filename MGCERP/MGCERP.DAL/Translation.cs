namespace MGCERP.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Translation
    {
        public Guid Id { get; set; }

        [StringLength(128)]
        public string CreatedByUserId { get; set; }

        public DateTime CreatedOn { get; set; }

        [StringLength(200)]
        public string EnglishName { get; set; }

        public int LanguageId { get; set; }

        public int ReferenceId { get; set; }

        [StringLength(200)]
        public string TranslatedText { get; set; }

        public int TranslationReferencePid { get; set; }

        [StringLength(128)]
        public string UpdatedByUserId { get; set; }

        public DateTime UpdatedOn { get; set; }

        public bool? IsActive { get; set; }
    }
}
