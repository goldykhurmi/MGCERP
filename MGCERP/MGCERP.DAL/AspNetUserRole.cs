namespace MGCERP.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AspNetUserRole
    {
        [Key]
        [Column(Order = 0)]
        public string UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string RoleId { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(128)]
        public string CreatedByUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(128)]
        public string UpdatedByUserId { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public virtual AspNetRole AspNetRole { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
    }
}
