namespace MGCERP.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MGCERP1 : DbContext
    {
        public MGCERP1()
            : base("name=MGCERP1")
        {
        }

        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRole> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<CompaniesCompanyModule> CompaniesCompanyModules { get; set; }
        public virtual DbSet<CompanyModule> CompanyModules { get; set; }
        public virtual DbSet<CompanyUser> CompanyUsers { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<MenusAspNetRole> MenusAspNetRoles { get; set; }
        public virtual DbSet<MenusCompanyModule> MenusCompanyModules { get; set; }
        public virtual DbSet<Translation> Translations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUserRoles)
                .WithRequired(e => e.AspNetRole)
                .HasForeignKey(e => e.RoleId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserRoles)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.CompanyUsers)
                .WithOptional(e => e.AspNetUser)
                .HasForeignKey(e => e.AspNetUsersId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Companies1)
                .WithOptional(e => e.Company1)
                .HasForeignKey(e => e.BillingCompanyId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Companies11)
                .WithRequired(e => e.Company2)
                .HasForeignKey(e => e.ParentCompanyId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.CompanyUsers)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Menu>()
                .Property(e => e.MenuName)
                .IsFixedLength();
        }
    }
}
