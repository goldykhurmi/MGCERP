namespace MGCERP.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AspNetUserLogin
    {
        [Key]
        [Column(Order = 0)]
        public string LoginProvider { get; set; }

        [Key]
        [Column(Order = 1)]
        public string ProviderKey { get; set; }

        [Key]
        [Column(Order = 2)]
        public string UserId { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(128)]
        public string CreatedByUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(128)]
        public string UpdatedByUserId { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
    }
}
