namespace MGCERP.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CompaniesCompanyModule
    {
        [Key]
        [Column(Order = 0)]
        public Guid CompanyId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid CompanyModuleId { get; set; }

        public bool IsActive { get; set; }

        [StringLength(128)]
        public string CreatedbyUserId { get; set; }

        [StringLength(128)]
        public string UpdatedByUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }
    }
}
