namespace MGCERP.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Language
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(50)]
        public string LanguageNative { get; set; }

        [Required]
        [StringLength(50)]
        public string LanguageCode { get; set; }

        [Required]
        [StringLength(50)]
        public string LanguageEnglishName { get; set; }

        public bool IsActive { get; set; }

        [StringLength(128)]
        public string CreatedbyUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(128)]
        public string UpdatedByUserId { get; set; }

        public DateTime? UpdatedOn { get; set; }
    }
}
