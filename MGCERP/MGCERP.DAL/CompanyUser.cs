namespace MGCERP.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CompanyUser
    {
        public Guid Id { get; set; }

        [StringLength(128)]
        public string AspNetUsersId { get; set; }

        public Guid CompanyId { get; set; }

        [StringLength(50)]
        public string EmployeeNumber { get; set; }

        public Guid? CompanyTitleId { get; set; }

        public int? GenderPid { get; set; }

        [Required]
        [StringLength(50)]
        public string Firstname { get; set; }

        [Required]
        [StringLength(50)]
        public string Lastname { get; set; }

        public Guid? LocationId { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? DepmartmentSubId { get; set; }

        public Guid LanguageId { get; set; }

        public DateTime? InDatetime { get; set; }

        public int? InOut { get; set; }

        public bool? IsEnablePremise { get; set; }

        public bool? IsEnableTimeFilter { get; set; }

        public bool? IsOnPremises { get; set; }

        public bool? IsResetPassword { get; set; }

        public bool? IsSyncProduction { get; set; }

        public bool? IsSupportUser { get; set; }

        public bool? IsSales { get; set; }

        public bool IsActive { get; set; }

        public DateTime? CurrentLoginDate { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public DateTime? LastLogOffDate { get; set; }

        public DateTime? OutDateTime { get; set; }

        [StringLength(128)]
        public string CreatedbyUserId { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(128)]
        public string UpdatedByUserId { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        public virtual Company Company { get; set; }
    }
}
